package com.test

class HomeController {

    def index() {
		def personList = Person.list()
		println "personList :" + personList
		[personList : personList]
	}
}
