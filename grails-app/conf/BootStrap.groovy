import com.test.Person

class BootStrap {

    def init = { servletContext ->
		new Person(lattitude : -25.363882, lagnitude : 131.044922).save(flush : true)
		new Person(lattitude : 23.00, lagnitude : 72.00).save(flush : true)
    }
    def destroy = {
    }
}
