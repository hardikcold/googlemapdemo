<!DOCTYPE html>
<%@page import="java.security.Permission"%>
<html>
<head>
<title>Geolocation</title>
<meta name="layout" content="main" />
<meta name="viewport" content="initial-scale=1.0, user-scalable=no">
<meta charset="utf-8">
<style>
html,body,#map-canvas {
	height: 100%;
	margin: 0px;
	padding: 0px
}
</style>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
<script>
function initialize() {

  var mapOptions = {
     zoom: 2
  }
  var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

  // Try HTML5 geolocation
  if(navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function() {
    	<g:each var="persondata" in="${personList}">
    		var myLatlng = new google.maps.LatLng(${persondata.lattitude}, ${persondata.lagnitude});
    		var marker = new google.maps.Marker({
      	      position: myLatlng,
      	      map: map,
      	      title : "${persondata.lattitude} ,${persondata.lagnitude} Location "
      	  });
    		map.setCenter(myLatlng);
    	</g:each>
    }, function() {
      handleNoGeolocation(true);
    });
  } else {
    // Browser doesn't support Geolocation
    handleNoGeolocation(false);
  }
}

function handleNoGeolocation(errorFlag) {
	 var content="";
	  if (errorFlag) {
	    content = 'Error: The Geolocation service failed.';
	  } else {
	    content = 'Error: Your browser doesn\'t support geolocation.';
	  }

	  var options = {
	    map: map,
	    position: new google.maps.LatLng(60, 105),
	    content: content
	  };

  	  var infowindow = new google.maps.InfoWindow(options);
      map.setCenter(options.position);
 }

 google.maps.event.addDomListener(window, 'load', initialize);
</script>
</head>
<body>
	<div id="map-canvas"></div>
</body>
</html>